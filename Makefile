.PHONY: 

default: semantics2015.pdf

%.pdf : %.tex
	latexmk -pdf $<

clean: 
	rm -f *.aux *.blg *.fls *.orig *.xmpi *.xmpdata *.abs *.log *.out

distclean: clean
	rm -f *.pdf *.dvi
