\begin{thebibliography}{10}

\bibitem{w3c:void}
K.~Alexander, R.~Cyganiak, M.~Hausenblas, and J.~Zhao.
\newblock Describing linked datasets with the {VoID} vocabulary.
\newblock {W3C} Interest Group Note, 3 March 2011.

\bibitem{buil-aranda-etal:2013}
C.~Buil-Aranda, A.~Hogan, J.~Umbrich, and P.-Y. Vandenbussche.
\newblock {SPARQL} web-querying infrastructure: Ready for action?
\newblock In {\em Proc. 12th Intl Semantic Web Conference
  (ISWC 2013), Sydney, Australia, October 21-25, 2013, Part II},
  LNCS 8219. Springer, 2013.

\bibitem{charalambidis-etal:2015a}
A.~Charalambidis, S.~Konstantopoulos, and V.~Karkaletsis.
\newblock Dataset descriptions for optimizing federated querying.
\newblock In {\em 24th Intl World Wide Web Conference Companion
  Proceedings {(WWW 2015)}, Poster Session, Florence, Italy, 18-22 May 2015},
  2015.

\bibitem{semagrow-d3.4}
A.~Charalambidis, A.~Troumpoukis, and J.~Jakobitsch.
\newblock {\em Techniques for heterogeneous distributed semantic querying.}
\newblock Semagrow Public Deliverable D3.4, 2015.

\bibitem{gorlitz:2011}
O.~G{\"{o}}rlitz and S.~Staab.
\newblock {SPLENDID:} {SPARQL} endpoint federation exploiting {VOID}
  descriptions.
\newblock In {\em Proc. 2nd Intl Workshop on Consuming
  Linked Data {(COLD 2011)}, Bonn, Germany},
  CEUR 782, 2011.

\bibitem{Graefe:1996}
G.~Graefe.
\newblock Iterators, schedulers, and distributed- memory parallelism.
\newblock {\em Softw., Pract. Exper.} 26(4), 1996.

\bibitem{Haas-et-al:1997}
L.~M. Haas, D.~Kossmann, E.~L. Wimmers, and J.~Yang.
\newblock Optimizing queries across diverse data sources.
\newblock In {\em Proc.
  23rd Intl Conference on Very Large Data Bases (VLDB'97),
  Athens, Greece}. 1997.

\bibitem{Hartig-et-al:2009}
O.~Hartig, C.~Bizer, and J.~C. Freytag.
\newblock Executing {SPARQL} queries over the web of linked data.
\newblock In {\em Proc. of 8th Intl Semantic Web Conference
  ({ISWC} 2009), Chantilly, VA, USA}. LNCS 5823. Springer, 2009.

\bibitem{Kossmann:2000}
D.~Kossmann.
\newblock The state of the art in distributed query processing.
\newblock {\em {ACM} Comput. Surv.}, 32(4):422--469, 2000.

\bibitem{lokers-stasinos-etal:2014}
R.~Lokers, S.~Konstantopoulos, A.~Stellato, R.~Knapen, and S.~Janssen.
\newblock Exploiting innovative linked open data and semantic technologies in
  agro-environmental modelling.
\newblock In {\em Proc. of the 7th Intl Congress on
  Environmental Modelling and Software {(iEMSs 2014)}}, San Diego, USA,
  15-19 June 2014.

\bibitem{saleem-et-al:2014}
M.~Saleem, Y.~Khan, A.~Hasnain, I.~Ermilov, and A.-C. {Ngonga Ngomo}.
\newblock A fine-grained evaluation of {SPARQL} endpoint federation systems.
\newblock Accepted to {\em Semantic Web Journal}. 2014.

\bibitem{Hibiscus}
M.~Saleem and A.-C. {Ngonga Ngomo}.
\newblock Hibiscus: Hypergraph-based source selection for {SPARQL} endpoint
  federation.
\newblock In {\em Proc. 11th {ESWC} Conference,
  Anissaras, Crete, Greece}, LNCS 8465. Springer, 2014.

\bibitem{DAW}
M.~Saleem, A.-C. {Ngonga Ngomo}, J. Xavier Parreira, H.~F. Deus, and M.~Hauswirth.
\newblock {DAW:} duplicate-aware federated query processing over the web of
  data.
\newblock In {\em Proc. 12th Intl Semantic Web Conference
  (ISWC 2013), Sydney, Australia, Part I},
  LNCS 8218, 2013.

\bibitem{schmidt-et-al:2011}
M.~Schmidt, O.~G\"{o}rlitz, P.~Haase, G.~Ladwig {\em et al.}
\newblock Fedbench: {A} benchmark suite for federated semantic data query
  processing.
\newblock In {\em Proc. of the 10th
  Intl Semantic Web Conference {(ISWC 2011)}, Bonn, Germany,
  Part {I}}, LNCS 7031. Springer, 2011.

\bibitem{schwarte-etal:2011}
A.~Schwarte, P.~Haase, K.~Hose, R.~Schenkel, and M.~Schmidt.
\newblock {FedX}: A federation layer for distributed query processing on
  {Linked Open Data}.
\newblock In {\em Proc. 8th Extended Semantic Web Conference
  {(ESWC 2011)}, Heraklion, Crete, Greece},
  LNCS 6644. Springer, 2011.

\end{thebibliography}
