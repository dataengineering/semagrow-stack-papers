\documentclass{beamer}


\mode<presentation>{
    %\usetheme{Frankfurt}
    \usetheme{Singapore}
    \usecolortheme{seagull}
    %\usecolortheme{rose}
    %\usecolortheme{dove}
    \usefonttheme{professionalfonts}
    \setbeamertemplate{navigation symbols}{}
%    \usepackage{lmodern}
%    \usepackage{fontspec}
%    \defaultfontfeatures{Mapping=tex-text}
    %\setsansfont[Ligatures={Common}]{Liberation Sans}
    %\setseriffont{Liberation Serif}
%    \setmonofont[Scale=0.8]{Monaco}
}

\usepackage{tikz}
\usetikzlibrary{arrows,shapes,fit}

\usepackage{wasysym}
\usepackage{listings}\lstset{language=SQL,morekeywords={PREFIX,java,rdfs,url}}
\usepackage[T1]{fontenc}


\newcommand{\term}[1]{\emph{#1}}
\newcommand{\algo}[1]{\textsc{#1}}
\newcommand{\join}{{$\Bowtie$}}
\newcommand{\bjoin}{{$\Bowtie_b$}}


\newtheorem*{remark}{Remark}
\newcommand{\quotes}[1]{`#1'}

\title{SemaGrow: \\Optimizing Federated SPARQL queries}
\author[Charalambidis et al]{
  A. Charalambidis
  \and A. Troumpoukis
  \and S. Konstantopoulos}

  \institute{
     Institute of Informatics and Telecommunications,
     NCSR \quotes{Demokritos}
     %\\ \email{\{acharal,konstant\}@iit.demokritos.gr}
  }
%\institute{Institute of Informatics and Telecommunications \\
           %NCSR Demokritos}
\date[SEMANTiCS15]{September 17, SEMANTiCS 2015, Vienna, Austria}

\hypersetup{%
  pdfauthor={Angelos Charalambidis, Antonis Troumpoukis and Stasinos Konstantopoulos},%
  pdftitle={SemaGrow: Optimizing Federated SPARQL queries},%
  pdfkeywords={Query processing, distributed databases},
  pdfsubject={SEMANTiCS presentation}
}

\begin{document}


\begin{frame}
\titlepage
\end{frame}

%\section{Introduction}

\begin{frame}
  \frametitle{Motivation}
  \begin{itemize}
    \item Ability to pose SPARQL queries that involve multiple data sources
    \begin{itemize}
      \item transparently: no need to know what to ask where
      \item efficiently: no need to worry about the execution
    \end{itemize}
    \item Approach: Federation of SPARQL endpoints
    \begin{itemize}
      \item communication using standard SPARQL
      \item live data access
    \end{itemize}
  \end{itemize}
\end{frame}


\begin{frame}
  \frametitle{Federated querying}
  \begin{itemize}
    \item Challenges
  \begin{itemize}
    \item loosely coupled query endpoints;
    \item independently maintained and populated;
    \item metadata might not be complete or updated.
  \end{itemize}
    \item Federated SPARQL querying systems:
    \begin{itemize}
      \item FedX, SPLENDID and many more
      \item FedX: Greedy and efficient in small scale queries
      \item SPLENDID: Quality execution plans but not as efficient
      \item The state of art has not reached a level of
            maturity as in traditional query processing
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{A simple federated SPARQL query}

  \begin{lstlisting}[basicstyle=\scriptsize]
  SELECT ?drug ?title {
    ?drug db:drugCategory dbc:micronutrient. (#P1)
    ?drug db:casRegistryNumber ?id.          (#P2)
    ?keggDrug rdf:type kegg:Drug.            (#P3)
    ?keggDrug bio2rdf:xRef ?id.              (#P4)
    ?keggDrug purl:title ?title.             (#P5)
  }
  \end{lstlisting}
\visible<2->{
    \begin{tikzpicture}[
  level distance=0.8cm,
  sibling distance=3.5cm,
  font=\scriptsize,
  align=center
      ]
      \node (root)  {\join \\ (28)}
    child {
      node {\join \\ (28)}
      child {
        node {\join \\ (381499)}
        child {
           node {\join \\ (47)}
           child { node{P1@DrugBank \\ (8)}}
           child { node{P2@DrugBank \\ (2240)}}
        }
        child {
    node {P3@KEGG \\ (8117)}
        }
      }
      child {
        node {P4@ChEBI,KEGG \\ (102343)}
      }
    }
    child {
      node {P5@ChEBI,KEGG \\ (68201)}
    }
      ;
    \end{tikzpicture}}
\end{frame}

\begin{frame}
    \frametitle{Alternative execution plans}

      \begin{tikzpicture}[
    level distance=0.8cm,
    sibling distance=3.5cm,
    font=\scriptsize,
    align=center
        ]
        \node (root)  {\join \\ (28)}
      child {
        node {\join \\ (28)}
        child {
          node {\join \\ (381499)}
          child {
             node {\join \\ (47)}
             child { node{P1@DrugBank \\ (8)}}
             child { node{P2@DrugBank \\ (2240)}}
          }
          child {
      node {P3@KEGG \\ (8117)}
          }
        }
        child {
          node {P4@ChEBI,KEGG \\ (102343)}
        }
      }
      child {
        node {P5@ChEBI,KEGG \\ (68201)}
      }
        ;
      \end{tikzpicture}
      \visible<2->{
      \begin{tikzpicture}[
    level distance=0.8cm,
    sibling distance=3.5cm,
    font=\scriptsize,
    align=center
        ]
        \node (root)  {\join \\ (28)}
      child {
        node {\join \\ (28)}
        child {
          node {\join \\ (164)}
          child {
      node {(P1\join P2)@DrugBank \\ (47)}
          }
          child {
      node {P4@ChEBI,KEGG \\ (102343)}
          }
        }
        child {
          node {P3@KEGG \\ (8117)}
        }
      }
      child {
        node {P5@ChEBI,KEGG \\ (68201)}
      }
        ;
      \end{tikzpicture}}
\end{frame}


\begin{frame}
  \frametitle{Phases of federated query processing}
  \begin{enumerate}
    \item Source selection
    \begin{itemize}
      \item Choose the data sources involved for each triple pattern
    \end{itemize}
    \item Query planning
    \begin{itemize}
      \item Decompose into smaller subqueries for each data source
    \end{itemize}
    \item Query execution
    \begin{itemize}
      \item Issue, receive and merge the results from different sources.
    \end{itemize}
  \end{enumerate}

  \begin{center}
    \tikzstyle{format} = [draw, thin, fill=gray!20, rounded corners=.8ex, inner sep=.3cm]
    \tikzstyle{format2} = [draw, thin, fill=green!30, rounded corners=.8ex, inner sep=.2cm]
    \begin{tikzpicture}[node distance=1.5cm, align=center, auto,>=latex', thick, font=\scriptsize]
      \path[->] node[format] (plan) {Query Planner};
      \path[->] node[format, below of=plan] (srcsel) {Source Selector}
            (srcsel) edge node {\tiny Sources} (plan);
      \path[->] node[format, right of=plan, right=.5cm] (exec) {Query Executor}
            (plan) edge node {\tiny Plan} (exec);
      \node[draw,draw=gray, thick, dashed,
      inner xsep=2em,
      inner ysep=.5em,fit=(plan) (srcsel) (exec)] {};
      \path[<->] node [format2, left of=exec, right=4.5cm] (sparql1) {SPARQL} (exec) edge node {} (sparql1);
      \path[<->] node [format2, below of=sparql1]  (sparql2) {SPARQL}(exec) edge node {} (sparql2);
      \path[<->] node [format2, below of=sparql2]  (sparql3) {SPARQL}(exec) edge node {} (sparql3);
      \path[->] node [left of=plan, left=1cm] (s) {} (s) edge node {\tiny Query} (plan);
    \end{tikzpicture}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Source selection}
  \begin{itemize}
    \item Pattern-wise source selection
    \item Hybrid approach:
    \begin{itemize}
      \item Use of metadata to facilitate source selection
      \item Additionally, ASK queries to verify existence of triples.
    \end{itemize}
    \item Metadata
    \begin{itemize}
      \item Enhanced VoID descriptions that may also contain statistics about the dataset.
      \item Also used for the estimation of the cardinality and selectivity for each pattern.
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Query planning}
  \begin{itemize}
    \item Dynamic programming algorithm to search for the optimal plan driven
          by a cost function
    \begin{itemize}
      \item Starts from single patterns
      \item Proceeds bottom-up build more complex subtrees
      \item Prunes inferior trees in every iteration
    \end{itemize}
    \item Cost estimation
    \begin{itemize}
      \item Models the effort and communication cost for each operator
      \item Depends on estimated cardinalities retrieved from the metadata
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \hspace{-1.2cm}
    \begin{tikzpicture}[
        auto,
        level distance=1.8cm,
        level 2/.style={sibling distance=5.5cm},
        level 3/.style={sibling distance=3cm},
        level 5/.style={sibling distance=3cm},
        level 6/.style={sibling distance=2.5cm},
        font={\tiny}, scale=.7,
        align=center]
      \node (root)  {$\pi_{\mbox{\tiny ?drug,?desc}}$}
        child {
  	node [left=.7cm]{\bjoin \\ {\tiny SITE: LOCAL} \\ {\tiny COST: 1730.3} \\ {\tiny CARD: 8} }
  	child {
  	  node {\bjoin \\ {\tiny SITE: LOCAL} \\ {\tiny COST: 1479.1} \\ {\tiny CARD: 4} }
  	  child {
  	    node [left=.1cm]{\bjoin \\ {\tiny SITE: LOCAL} \\ {\tiny COST: 777.4} \\ {\tiny CARD: 13} }
  	    child {
  	      node{SourceQuery \\ {\tiny SITE: LOCAL} \\ {\tiny COST: 375.4} \\ {\tiny CARD: 8} }
  	      child{
  		node [left=.1cm]{\join \\ {\tiny SITE: DrugBank} \\ {\tiny COST: 324.7} \\ {\tiny CARD: 8} }
  		child {
  		  node {P1 \\ {\tiny SITE: DrugBank} \\ {\tiny COST: 7.0} \\ {\tiny CARD: 7} }
  		}
  		child {
  		  node {P2 \\ {\tiny SITE: DrugBank} \\ {\tiny COST: 2240.0} \\ {\tiny CARD: 2240} }
  		}
  	      }
  	    }
  	    child {
  	      node {$\cup$ \\ {\tiny SITE: LOCAL} \\ {\tiny COST: 115773.6} \\ {\tiny CARD: 105158} }
  	      child {
  		node[right=.3cm]{SourceQuery \\ {\tiny SITE: LOCAL} \\ {\tiny COST: 35208.0} \\ {\tiny CARD: 31962} }
  		child {
  		  node {P4 \\ {\tiny SITE: ChEBI} \\ {\tiny COST: 31962.0} \\ {\tiny CARD: 31962} }
  		}
  	      }
  	      child {
  		node[right=.2cm]{SourceQuery \\ {\tiny SITE: LOCAL} \\ {\tiny COST: 80565.6}  \\ {\tiny CARD: 73196} }
  		child {
  		  node {P4 \\ {\tiny SITE: KEGG} \\ {\tiny COST: 73196.0} \\ {\tiny CARD: 73196} }
  		}
  	      }
  	    }
  	  }
  	  child {
  	    node{SourceQuery \\ {\tiny SITE: LOCAL} \\ {\tiny COST: 8978.7} \\ {\tiny CARD: 8117} }
  	    child {
  	      node {P3 \\ {\tiny SITE: KEGG} \\ {\tiny COST: 8117.0} \\ {\tiny CARD: 8117} }
  	    }
  	  }
  	}
  	child {
  	  node {$\cup$ \\ {\tiny SITE: LOCAL} \\ {\tiny COST: 75121.1} \\ {\tiny CARD: 68201} }
  	  child {
  	    node{SourceQuery \\ {\tiny SITE: LOCAL} \\ {\tiny COST: 37510.5} \\ {\tiny CARD: 34055} }
  	    child {
  	      node {P5 \\ {\tiny SITE: ChEBI} \\ {\tiny COST: 34055.0} \\ {\tiny CARD: 34055} }
  	    }
  	  }
  	  child {
  	    node{SourceQuery \\ {\tiny SITE: LOCAL} \\ {\tiny COST: 37610.6} \\ {\tiny CARD: 34146} }
  	    child {
  	      node {P5 \\ {\tiny SITE: KEGG} \\ {\tiny COST: 34146.0} \\ {\tiny CARD: 34146} }
  	    }
  	  }
  	}
        }
      ;
    \end{tikzpicture}
  \end{frame}

\begin{frame}
  \frametitle{Query execution}

  \begin{itemize}
    \item Query execution is realized as a tree where streams are flowing from
          the sources and processed by the nodes.
    \item Reactive streams
    \begin{itemize}
      \item Data-driven fashion: instead of demanding data, process as soon as data are available.
      \item Asynchronously: nodes are not block-waiting for data.
      \item Concurrently: Each node in the execution tree can work independently as soon as data are available.
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]
  \frametitle{Query execution}
  \begin{itemize}
  \item Bind Join implementation
  \begin{itemize}
    \item Similar to nested loop join but for each binding a new query is created.
    \item Optimization: create a query for a batch of bindings:
    \begin{itemize}
      \item VALUES structure in SPARQL 1.1
      \item UNION  structure in SPARQL 1.0
    \end{itemize}
  \end{itemize}
  \end{itemize}

\begin{center}
  \begin{tikzpicture}[
level distance=0.8cm,
sibling distance=3.5cm,
font=\scriptsize,
align=center
    ]
    \node (root)  {\bjoin \\ (28)}
  child {
  node (p2) {(P1\join P2)@DrugBank \\ (47)}
      }
      child {
  node (p3) {P4@ChEBI,KEGG \\ (102343)}
      }
    ;
    \path [->] (p2) edge [bend left=60, dashed] (p3);
    \node (u) [below of=p3, right=.1cm] {
      \begin{lstlisting}[basicstyle=\tiny]
        SELECT ?id ?keggDrug
        { ?keggDrug bio:xRef ?id . }
        VALUES (?id) {
          (cas:70-26-8)
          (cas:56-85-9)
          (cas:61-19-8) }
      \end{lstlisting}
    } ;
  \end{tikzpicture}
\end{center}
\end{frame}

%\section{Evaluation Experiments}

\begin{frame}
  \frametitle{Evaluation}

  \begin{itemize}
    \item FedBench benchmark suite
    \begin{itemize}
      \item Designed mainly for evaluating SPARQL federated query processing systems.
      \item FedX and SPLENDID most performant in FedBench benchmarks.
      \item 14 queries from Cross Domain (CD) and Life Science (LS) sets.
      \item Federation with 5 (CD) and 4 (LS) data sources.
      \item Queries vary in complexity, size, structure and sources involved.
    \end{itemize}
    \item Experimental setup
    \begin{itemize}
      \item Data sources deployed on separate triple stores (Virtuoso)
      \item Data sources are close to the federators: no network delays
      \item Average of 5 runs.
    \end{itemize}
  \end{itemize}

\end{frame}

\begin{frame}
  \frametitle{Competitors}
  \begin{itemize}
    \item FedX
    \begin{itemize}
      \item Source selection using ASK queries to verify existence of triples and
            cache the answers for performance.
      \item Query planning without statistics using a fast and greedy algorithm.
      \item Multi-threaded query execution to increase performance.
    \end{itemize}
    \item SPLENDID
    \begin{itemize}
      \item Source selection using both VoID metadata and ASK queries.
      \item Query planning using dynamic programming and statistics derived from the VoID metadata.
      \item Block-waiting execution engine.
    \end{itemize}
  \end{itemize}
\end{frame}


\begin{frame}
\frametitle{Evaluation Results}
\begin{center}
\begin{tabular}{lrrr}
  \hline
  Query & FedX & SPLENDID & SemaGrow  \\
  \hline
  CD1 & 23 & 76 & 18 \\
  CD2 & 14 & 34 & 12 \\
  CD3 & 32 & 81 & 38 \\
  CD4 & 35 & 43 & 50 \\
  CD5 & 31 & 58 & 87 \\
  CD6 & 400 & 8119 & 385 \\
  CD7 & 484 & 1644 & 404 \\
  \hline
  LS1 & 36 & 54 & 63 \\
  LS2 & 31 & 189 & 36 \\
  LS3 & 3821 & 24468 & 2935 \\
  LS4 & 32 & 70 & 84 \\
  LS5 & 2287 & 295147 & 1061 \\
  LS6 & 95983 & 19670 & 107 \\
  LS7 & 1683 & 18634 & 1186 \\
  \hline
\end{tabular}
\end{center}
\end{frame}

%\section{Conclusions}

\begin{frame}
  \frametitle{Ongoing and future work}
  \begin{itemize}
    \item Experiment with existing join-aware source selection (e.g. HiBiSCuS).
    \item Optimize SPARQL queries with extensions (e.g. spatial, temporal).
    \item Perform experiments with other benchmark suites than FedBench
    \begin{itemize}
      \item more complex queries
      \item bigger datasets (bigRDF bench)
      \item real world experiments: network delays, source unavailability
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Preliminary results on more complex queries}
  \begin{center}
  \begin{tabular}{lrrr}
    \hline
    Query & FedX & SemaGrow  \\
    \hline
    C1 & 150002 & 20561  \\
    C2 & 7497 & 6440  \\
    C3 & 41624 & 17940 \\
    C4 & 230313 & 52949 \\
    C5 & timeout & 415284 \\
    C6 & 83524 & 51454 \\
    C7 & 989 & 592  \\
    C8 & 5680 & 23822 \\
    C9 & timeout & 385 \\
    C10 & 26751 & 4010 \\
    \hline
  \end{tabular}
  \end{center}
\end{frame}

\begin{frame}
  \frametitle{Conclusions}
  \begin{itemize}
    \item We are proposing SemaGrow, an engine that federates SPARQL endpoints
    \item We have discussed the query processing methods used in SemaGrow
    \item We have evaluated the performance using FedBench in comparison to
          the state of art systems.
  \end{itemize}
\end{frame}

\begin{frame}

  \vspace{40pt}

  \begin{center}
    {\Huge Questions?}
    \\[3cm]
    {You can find us on GitHub: \\\url{http://www.github.com/semagrow}}\\
    \centering\includegraphics[scale=.3]{../figures/semagrow_logo}
  \end{center}
\end{frame}


\end{document}
