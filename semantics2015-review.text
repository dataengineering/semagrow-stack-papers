

----------------------- REVIEW 1 ---------------------
PAPER: 108
TITLE: SemaGrow: Optimizing Federated SPARQL queries
AUTHORS: Angelos Charalambidis, Antonis Troumpoukis and Stasinos Konstantopoulos

OVERALL EVALUATION: 1 (weak accept)
REVIEWER'S CONFIDENCE: 4 (high)
APPROPRIATENESS: Does this paper fit the SEMANTiCS Research Track?: 5 (Certainly)
CLARITY: Is the paper well-written and well-structure? Are the paper's ideas clear?: 4 (Unterstandable by most readers)
ORIGINALITY: Is the paper novel? Does it improve previous approaches considerably?: 3 (Nice research contribution)
IMPACT: How significant is the work described? Does the paper present a significant and important advance?: 3 (Interesting, but not too influential.)
EVALUATION: To what the extend has the approach been evaluated?: 3 (The approach has been tested and evaluated to a limited extent)
Consider for the Posters & Demos Track? (The Posters & Demos looks for innovative work in progress, late-breaking results, and smaller contributions (including pieces of code). For more details see the call at http://semantics.cc/open-calls): 1 (No)

----------- REVIEW -----------

This paper presents SemaGrow, a federated engine for SPARQL query
federation over multiple endpoints. SemaGrow is an index-assisted
approach based on VoID descriptions of the SPARQL endpoints. The index
contains the set of distinct predicates for all data sources (SPARQL
endpoints). For each distinct predicate p, it stores the total number
of triples, the total number of distinct subjects, and total number of
distinct objects containing predicate p. SemaGrow makes use of the
hybrid approach (i.e., using SPARQL ASK queries and Index) for source
selection. The triple patterns cardinalities are estimated based on
the information store in the index and whether the subject, object of
the triple pattern is bound or not.  The join (between two triple
patterns) cardinality is estimated based on the cardinalities of the
participating triple patterns. A dynamic programming approach is used
for query planning, a reactive query execution (instead of iterative
model) combined with Bind and Merge joins is used for query results
retrieval.  The experimental results suggests (according to authors)
that SemaGrow has comparable performance with state-of-the-art engine
FedX, and better performance than SPLENDID.

Overall, I like the approach and it can definitely be a nice addition
to the SPARQL endpoint federation engines. The paper is easy to read
and well-motivated with a running example. Some of the concerns that
should be addressed in the final version are:

1. Cost Estimation: The cost estimation is only explained for triple
patterns with bound predicates. Since the index only stores statistics
for distinct predicates, how would you estimate the cost for triple
patterns with unbound predicates? Similarly, how would you estimate
the cost for joins between two triple patterns in which at least one
contains unbound predicate?

2. Source Selection: ON page 2 the author wrote "SemaGrow uses the
same hybrid, pattern-wise source selection method as SPLENDID
does". However, on page 7, section 4.3 "These heuristics helps
SPLENDID to produce an efficient plan, but are not safe in terms of
completeness of the query. For example, consider the triple patterns
of the form of ?x owl:sameAs ?y, that occur in every source. FedX and
SemaGrow ask every source, while SPLENDID perform sameAs
groupings". If I correctly understood, this shows that the SemaGrow's
source selection is different than SPLENDID. Thus, the source
selection algorithm should be added in the paper or the author should
clearly mention the differences to that of SPLENDID.

3. Experimental Setup: I am not convinced by the current experimental
setup, i.e., run all of the nine Fedbench's SPARQL endpoints (as a
separate instances) on a single machine with only 4GB of RAM.  It
would be better to assign nine physical machines, one per endpoint. A
local, dedicated network setup can be used to minimize the network
delay.

4. Evaluation: A batch size of 8 was "optimally" chosen after running
FedBench's LS queries (ref. Table 2).  The question here is which
batch size was set for FedX? By default it is set to 15. The
evaluation results are biased (in my opinion) if FedX is used in
default settings, as SemaGrow is first tuned according to the
benchmark queries, before running the benchmark.

5. Comparison: As SemaGrow implements non-blocking operator to support
adaptive query processing. It should be compared to ANAPSID, a
state-of-the-art adaptive query processing engine that implements
non-blocking operator.

6. Iterative vs. Reactive query Execution: The iterative model is
criticized "Requests that are propagated down the stream can be
blocked by other slower operations and potentially stalling the
overall query evaluation".

The author mentioned "The reactive model of computation seems ideal
for use cases such as federation over wide area networks where data
are produced in different rates and unexpected delays can occur and in
the same time, the federated sources can work truly in parallel".  A
reference or initial experimental results should be added to support
above claims.  The author mentioned FedX partially solve this problem
by using mutli threaded execution engine. However, threads should be
synchronized resulting in additional cost.  However, on section 4.2
"both FedX and SemaGrow exhibit similar behaviour. In particular, the
query execution time is similar for both engines in the situations
where both optimizers produce the same plan".  Does this mean that the
thread synchronization cost is almost negligible? If yes, do we really
need the reactive query model?

7. Comparison with SPLENDID: The author mentioned in few places that
the reason for our small execution time is due to the efficient query
execution engine of SemaGrow. The actual reason should be
provided. For example, both SPLENDID and SemaGrow uses the same index,
same source selection (according to author), same dynamic programming
strategy. As such the author should clearly mention the reason for
SemaGrowâ€˜s better performance when comparing to SPLENDID.

8. Uniform Communication Cost: Since all the endpoints were started on
a single machine, do we really need to add a "uniform 10%"
communication cost? How a uniform cost would affect the query
planning? It would be nice to consider a variable cost, based on
stored network statistics.

Some of the minor comments are:

1. FedBench contains 11 queries (named LD) to test Linked Data
Federation engines also called Link traversal engines.  The author
should clearly state that SemaGrow is SPARQL endpoint federation
engine thus we only considered the relevant, i.e., the LS, CD queries
of FedBench.

2. The abstract is too short and missing the core evaluation results
or conclusion drawn.

3.	â€œthat the state of the art has not reached a satisfactory level of maturityâ€  reference is needed. 

4. What is the unit of the runtimes presented in Tables 5-7?  I assume
they are in mili seconds. This should be mentioned.

5. Since the results are presented in tables, it would be nice to
statistically analyze the results (e.g., by using wilcoxon signed rank
test, t-test etc) and report the significant improvements.

6. May be table 3, table 4 can be removed (since these statistics are
already provided at FedBench) and a source selection algorithm can be
added in to the paper.

7. The current triple pattern-wise source selection algorithm can
further be extended to "join-aware" source selection as
implemented in ANAPSID, HiBISCuS etc. This might results in better
plan selection.

8. FedX version should be mentioned, as FedX3.0 seems to be more
efficient.

Overall, an interesting paper that needs some in-depth experimental
evaluation (on more complex queries) in order to get the actual
performance improvements.


----------------------- REVIEW 2 ---------------------
PAPER: 108
TITLE: SemaGrow: Optimizing Federated SPARQL queries
AUTHORS: Angelos Charalambidis, Antonis Troumpoukis and Stasinos Konstantopoulos

OVERALL EVALUATION: -1 (weak reject)
REVIEWER'S CONFIDENCE: 4 (high)
APPROPRIATENESS: Does this paper fit the SEMANTiCS Research Track?: 5 (Certainly)
CLARITY: Is the paper well-written and well-structure? Are the paper's ideas clear?: 4 (Unterstandable by most readers)
ORIGINALITY: Is the paper novel? Does it improve previous approaches considerably?: 2 (Marginal extensions over existing work)
IMPACT: How significant is the work described? Does the paper present a significant and important advance?: 2 (Marginal impact.)
EVALUATION: To what the extend has the approach been evaluated?: 3 (The approach has been tested and evaluated to a limited extent)
Consider for the Posters & Demos Track? (The Posters & Demos looks for innovative work in progress, late-breaking results, and smaller contributions (including pieces of code). For more details see the call at http://semantics.cc/open-calls): 2 (Maybe)

----------- REVIEW -----------

The authors present in the work SemaGrow, a federated SPARQL query
engines which claims to optimise the query evaluation based on a
detailed cost model and by using state of the art stream processing
techniques.

== Strong points ==

*) In general, the paper is very well written, structured and the presentation is very clear

*) Using stream processing techniques are well motivated

== Weak points == 
*) Related work is missing ( the authors should discuss more timely work (e.g. 
	*) DAW: Duplicate-AWare Federated Query Processing over the Web of Data by Muhammad Saleem, Axel-Cyrille Ngonga Ngomo, Josiane Xavier Parreira, Helena Deus, and Manfred Hauswirth in Proceedings of ISWC2013
	*) HiBISCuS: Hypergraph-Based Source Selection for SPARQL Endpoint Federation by Muhammad Saleem and Axel-Cyrille Ngonga Ngomo in Extended Semantic Web Conference (ESWC 2014)
	*) Maribel Acosta:
A Hybrid Approach to Perform Efficient and Effective Query Execution Against Public SPARQL Endpoints. WWW (Companion Volume) 2015
	*) Maribel Acosta, Maria-Esther Vidal, Fabian FlÃ¶ck, SimÃ³n Castillo, Carlos Buil Aranda, Andreas Harth:
SHEPHERD: A Shipping-Based Query Processor to Enhance SPARQL Endpoint Performance. International Semantic Web Conference (Posters & Demos) 2014: 

*) The example query should be introduced in more detail and better explained in the examples. Readers not familiar with the FedBench queries and framework will have problems in understanding the challenges and the details of the query. 
*) It is not clear which SPARQL operators are supported ( what about optionals and Filters? ) 

Overall, the paper is well written and motivated but strongly lacks in
presenting the contribution of the paper. The authors propose an
approach which combines the query planning approaches from SPLENDID
and FedX without really advancing their approaches and improving their
performance ( as shown in the evaluation).

The new query processing pipeline seems to bring an advantage over the
iterator model of SPLENDID but does not outperform FedX. This is also
due to the fact that FedX generates Bushy Tree query plans which can
be executed in parallel.

I think that the current state of the paper is not mature enough to be
accepted as a full conference contribution.


Section 1:

I really like the introduction which is very clear and motivates the
problem and work well.

Section 2:

Section 2.1: is well introduced but could benefit from explaining the
example query in more detail. Somehow, it is assumed that readers now
the Fedbench and Life Science domain to understand the query and query
patterns.

Section 2.2: It is unclear how the authors get the number of distinct
subjects, predicate and objects. Are these taken from the VoiD
descriptions? If so, how would the cost model work in the absence of
VoiD descriptions?

It would be also better to present actual formula and how the
necessary values are collected ( VoiD, ASK queries).

In addition, it is well-known that VoiD has limitations for query cost
estimation, e.g., for path queries as the authors state. How do the
authors try to improve this?
	
In general, it would be good to present that detailed cost formula for
the various operations and how the necessary values are collected.

How do the authors deal with OPTIONALs and FILTER operators? 


Section 3: 
The authors should consider the results about federated join evaluation presented by 

Bruil-Aranda, Carlos, Polleres, Axel, Umbrich,
Jurgen. 2014. Strategies for executing federated queries in
SPARQL1.1. In Proceedings of the 13th International Semantic Web
Conference (ISWC 2014), which shows that certain distributed join
evaluation strategies produce incorrect results depending on the query
shape and in case blank nodes are involved.
	
Section 4: 

Section 4.2: Add the unit of the presented values. I assume it is
seconds.  Maybe a stack-bar plot would save space here and present the
results in a better way to distinguish between query execution time
and query planning time.  it would be also interesting to indicate for
which queries the query plans are identical and for which they are
different.

In general, it seems that SemaGrow produces the same query plans as
FedX and the query performance seems to be very similar, indicating
that the optimisations do not really improve the state of the art
query processing without statistics.  One would expect that SemaGrow
shows much better performance than FedX due to the use of additional
computed statistics which is not validated by the results.

Section 4.3:
FedX is general the fastest engine for query planning which also
produces the same plans as semagrow. As such I do not see the added
benefit of using statistics from the SemaGrow approach.

In general, the evaluation results are not really showing that the
SemaGrow is advancing the state-of-the-art query planning and
processing of federated engines

Section 5: 

The authors claim that semaGrow produces optimal query plans in most
cases. How was this claim evaluated considering the underlying
complexity of the query planning ( pushing joins to the remote
endpoint, performing distributed joins with bind joins etc..,
execution order of endpoints...)?

I am not aware of any study which reports on optimal query plans for
the FedBench framework.


----------------------- REVIEW 3 ---------------------
PAPER: 108
TITLE: SemaGrow: Optimizing Federated SPARQL queries
AUTHORS: Angelos Charalambidis, Antonis Troumpoukis and Stasinos Konstantopoulos

OVERALL EVALUATION: 2 (accept)
REVIEWER'S CONFIDENCE: 4 (high)
APPROPRIATENESS: Does this paper fit the SEMANTiCS Research Track?: 5 (Certainly)
CLARITY: Is the paper well-written and well-structure? Are the paper's ideas clear?: 5 (Very clear)
ORIGINALITY: Is the paper novel? Does it improve previous approaches considerably?: 3 (Nice research contribution)
IMPACT: How significant is the work described? Does the paper present a significant and important advance?: 4 (Significant advances over previous approaches.)
EVALUATION: To what the extend has the approach been evaluated?: 4 (The approach has been evaluated on a reasonable corpus or with a small set of users)
Consider for the Posters & Demos Track? (The Posters & Demos looks for innovative work in progress, late-breaking results, and smaller contributions (including pieces of code). For more details see the call at http://semantics.cc/open-calls): 3 (Yes)


----------- REVIEW -----------

This paper presents SemaGrow, a federated SPARQL query engine that
optimizes query plans using estimated statistics and executes these
query plans with asynchronous and non-blocking components. The authors
very clearly explain their approach. The evaluation results show that
SemaGrow outperforms FedX and SPLENDID for queries of the FedBench
benchmark. Both the generation of optimizations and the actual
execution are faster. This paper is very well written and I recommend
acceptance. Some minor issues (structure, dedicated related work,
clarifications, abstract, language) should be addressed.


Overall, I would like to thank the authors for the very clear writing
style. You really took the time to explain things, which will greatly
benefit your readers.


ABSTRACT

The abstract disappoints: whereas the "what" is clear, I recommend
adding more context and an explicit problem definition. What is the
problem you are solving? Why are existing solutions not sufficient?
Where is your solution different? You can use more words to make these
things clear. The current text is also quite vague: "state-of-the-art
stream processing technologies" => you actually mean "non-blocking &
asynchronous". The more important lesson, that you improve the
optimization/execution time trade-off, is not mentioned. Instead of
"[we] offer conclusions" is not all that helpful: just say instead
what you conclude. A strong paper like this really deserves an equally
strong abstract.



INTRODUCTION

The "the implication is that [metadata] is needed" is not
correct. Metadata might indeed help, but there are other things. You
can rephrase this as "[metadata] helpsâ€¦".

"[non-]satisfactory level of maturity" needs a citation.

The sentence "In fact, federated endpoints..." does not really add to
the paragraph or the discussion here.

I would recommend to, just briefly, explain "federated querying" and
"distributed triple stores". In contrast to other sections of the
paper, these concepts are not explained/defined.

The sentence "The observation that..." is too complicated to grasp at
a first time. Its contents are highly relevant though. Please split
this into two sentences. (And also mention this observation in the
abstract.)

In general, be clear what the "performance" is you are optimizing
for. It's clearly query time you are after. Another optimization for
"performance" is to minimize the server load, so the server performs
better with many clients (but queries might be slower).

I would recommend to move footnote 1 into the introduction: already
say that you're using FedBench during the mention of the structure.


SECTION 2

Related work is loosely mentioned throughout this section; the paper
has no dedicated section for this. As a result, the structure is a bit
vague, and some highly related systems are missing. For instance,
"source selection" is not explicitly mentioned as an important step in
the query process (even though Semagrow does it), and works like
HiBISCus deserve a mention here.

Clearly mention what you are optimizing for, because there are many
parameters one can optimize for. I would suggest renaming the section
into "Optimizing query execution times".

The example is very instructive. Figure 2 could perhaps be clarified
with more descriptive subcaptions. Instead of simply "naive execution
plan", you can say "this execution plan is naive, because a
non-selective pattern operation is placed in the beginning".

"Semagrow uses the same [...] method as SPLENDID does." => explictly
mention why, even though it might be logical.

Same principle as before "effective heuristic" => for what is it
effective? To minimize query time (not to minimize server load). Be
explicit here. Optimization is not a one-dimensional thing, one can
optimize for many parameters.

During the example, you mention "multiplicative results". Maybe the
actual numbers would be really instructive here, as the difference is
huge?

The paragraph "The optimal plan" was a bit confusing. First you say
that it should be a soft constraint, but then you place it as a
default constraint anyway. I eventually understood, but this should be
clearer. Also, the reasoning is interesting: maybe the fact that it
does not occur is actually benchmark bias? In that regard, it is
instructive you included the example in the previous paragraph.

Please don't call Sesame's notation "standard", because it is
not. Just refer to it as "Sesame's syntax", and then you can remove
the footnote.

In the context of "different overhead to different datasources", I
think you might like the work on SHEPHERD
(http://ceur-ws.org/Vol-1272/paper_147.pdf). This framework measures
the overhead of different features per datasources, which even
influenced the execution time of non-federated queries.

When you said "By assuming independence...", I was wondering "are
these assumptions realistic?", but you answered that question at the
end of the paragraph, which is nice. Maybe you can already make that
clear through "By (optimistically) assuming...".

Terms like "grounding star patterns" and "filler instances" should be
defined/explained.

Section 2.3 I didn't understand. This is the only section (apart from
the abstract), which I would really ask you to rewrite. Also, it is
very important for algorithms that, instead of just the "what", you
also explain the "why". Why are you doing these steps? Why does it
lead to what you want? Please answer these questions carefully in the
text, as the algorithm is an important point.


SECTION 3

While I appreciate the reactive idea, it is not entirely novel. First
of all, the synchronous interpretation of the next() operation is
programming language-dependent. E.g., in Java, it would indeed be
natural to implement it in a blocking, synchronous way, so you
actively wait for a HTTP request to return. In other
languages/platforms, such as JavaScript/Node.js, you cannot actively
wait on an HTTP request and you have to implement next() in an
asynchronous way. The limitation is thus not in the iterator idea, but
in a specific implementation.

An implementation I made myself
(http://linkeddatafragments.org/publications/iswc2014.pdf /
https://github.com/LinkedDataFragments/Client.js) also has
non-blocking, asynchronous iterators. Because of the underlying
Node.js platform, this client is also data-driven/reactive.

So instead of the "reactive paradigm", I would just call things
"asynchronous, non-blocking iterators", and acknowledge that this
technique has already been applied. Blocking avoidance is also the
main topic of a Linked Data query execution paper
(https://cs.uwaterloo.ca/~ohartig/files/HartigEtAl_QueryTheWeb_ISWC09_Preprint.pdf).

The first paragraph of 3.2 is written around bind join and nested loop
join, but this explanation is a bit confusing. Proper definitions of
both techniques would be much more helpful.

The last two paragraphs of 3.2 seem to belong in the evaluation, as
they measure optimal parameters. However, because they are currently
mentioned in 3.2, the test setup etc. are missing. Logically, this
belongs in the evaluation. However, this also means that the structure
needs to be made clear up front: the evaluation will then a) find
optimal parameters b) measure query time with these parameters.

The explanation of VALUES versus UNION was helpful. However, the
difference in performance is likely an implementation bug (not even
something unoptimized), as the purpose of VALUES was precisely to
avoid the equivalent UNION query. The authors do acknowledge this, but
do not mention what endpoint they tested.


SECTION 4

Again, be sure to precisely define what "performance" means; it would
be better to talk about "query time" altogether.

I was a bit disappointed that the tests were run with a single machine
emulating multiple endpoints. This gives a lot of potential noise in
the results, as requests to "one" endpoint influence "another". You
should really set up multiple machines in follow-up
publications. Personally, I can recommend a virtual machine system for
this like Amazon, which we used for our FedBench tests.

To what extent is "reliable and fast" a realistic assumption on the
Web? (Sadly, it's not, in my experience.)

"SPARQL protocol" => which version?

I would suggest to merge Tables 5-7 in one large table. After all, the
row names are the same, and this would allow easier comparisons.

Please do mention the unit of time! Seconds? Milliseconds?  If
milliseconds, measuring up to 1ms seems too specific.  Maybe mention
the standard deviation?

In the tables, could you also right align numbers? This allows for
easier scanning as well. Otherwise, the tables are clear, without
visual noise (lines etc.).  Maybe consider highlighting the best score
of each row?

In 4.3, I didn't fully understand "sameAs groupings." What groupings
exactly?  The remark about completeness is very important though,
thanks for that.

Results are very well analyzed.

In the end, also nuance "optimality", because this is relative to one
parameter.


SECTION 5

Thanks for writing an actual conclusion and not a summary. This is
very insightful!

The sentence "As a consequence, SPLENDID..." is redundant with
"Another important distinction". Just call it a "disadvantage" already
the first time.

I would suggest to replace footnote 5 with an actual reference to
the work
(http://link.springer.com/chapter/10.1007%2F978-3-642-41338-4_18).

In the context of the cost model, you might again like SHEPHERD's
assessment of endpoints.

I look very much forward to see the influence of estimation
accuracy. This is highly relevant future work!

Similarly, the influence of real-world factors would be very
meaningful. It's my personal opinion though that public SPARQL
endpoints might be too unreliable for federated queries, but that
remains to be (dis)proven then ;-)



LANGUAGE

- Replace words such as "enormously" and "extremely" by more
  emotion-neutral, and if possible quantifiable, expressions.
- "planning overheads" => "planning overhead"
- "this heuristics" => "these heuristics" or "this heuristic".
- "Bind Join" => "bind join"
- "SPLENDID perform" => "SPLENDID performs"
- "result to the construction" => "results in the construction"
- "datatbses" => "databases"



Reviewed by Ruben Verborgh <ruben.verborgh@ugent.be>.


-------------------------  METAREVIEW  ------------------------
PAPER: 108
TITLE: SemaGrow: Optimizing Federated SPARQL queries

RECOMMENDATION: accept

The authors present in the work SemaGrow, a federated SPARQL query
engines which claims to optimise the query evaluation based on a
detailed cost model and by using state of the art stream processing
techniques.

All reviewers agree that this is an interesting, innovative and well
written high quality paper. It has provoked much discussion.  The
authors should note that their paper "just" above the threshold for
acceptance. We expect the comments in the detailed reviews to be
addressed as much as possible in the camera ready version if not
certainly at presentation time.

The authors should remedy in particular the following comments:

1) Section 4.3- "The evaluation results are not really showing how
SemaGrow is advancing the state-of-the-art" See Reviewer 2"

2) More details are required in the experimental setup and evaluation
section (See Reviewer 1 comments)

2) Missing Related work See Reviewer 2

3) A detailed cost formula for the various operations, See Reviewer 1 and 2

4) All general minor comments from Reviewers 1,2,3 should be fixed.

